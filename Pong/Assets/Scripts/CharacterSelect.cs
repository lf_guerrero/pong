﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterSelect : MonoBehaviour
{
    public string playerAxis = "Vertical";
    public string playerBoost = "PlayerBoost";
    private float inputDelay = 0.5f;

    public GameObject playerSelector;
    public GameObject playerCharacter;
	public GameObject opponentCharacter;
    public Vector2 selectedPos;
    public Sprite[] characters;
    public Vector2 pos01, pos02, pos03;
    public int playerPos;
    public string pickedCharacter;
    private bool alreadyPicked;

	[HideInInspector]
	public bool initOnline;

    void Awake()
    {
        playerSelector.transform.position = pos01;
        playerPos = 1;
        alreadyPicked = false;
    }
    void Update()
    {
        inputDelay += Time.deltaTime;

        if (Input.GetButtonDown(playerBoost))
        {
            if (playerPos == 1 && !GameManager.pInstance.dochinoIsPicked)
            {
				alreadyPicked = true;
				pickedCharacter = "Dochino";

				playerCharacter.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
				playerCharacter.transform.position = Vector2.Lerp(playerCharacter.transform.position, selectedPos , 1);
				GameManager.pInstance.dochinoIsPicked = true;
				GameManager.pInstance.dochinoSelection.color = new Color(0.3f, 0.3f, 0.3f, 0.75f);
            }
            else if (playerPos == 2 && !GameManager.pInstance.florzinhaIsPicked)
            {
				alreadyPicked = true;
				pickedCharacter = "Florzinha";

				playerCharacter.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
				playerCharacter.transform.position = Vector2.Lerp(playerCharacter.transform.position, selectedPos, 1);
				GameManager.pInstance.florzinhaIsPicked = true;
				GameManager.pInstance.florzinhaSelection.color = new Color(0.3f, 0.3f, 0.3f, 0.75f);
            }
            else if (playerPos == 3 && !GameManager.pInstance.lindinhaIsPicked)
            {
				alreadyPicked = true;
				pickedCharacter = "Lindinha";

				playerCharacter.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
				playerCharacter.transform.position = Vector2.Lerp(playerCharacter.transform.position, selectedPos, 1);
				GameManager.pInstance.lindinhaIsPicked = true;
				GameManager.pInstance.lindinhaSelection.color = new Color(0.3f, 0.3f, 0.3f, 0.75f);
            }
        }

        if(pickedCharacter != null)
        {
			if (playerSelector.name == "PlayerSelector01")
                GameManager.pInstance.playerCharacter01 = pickedCharacter;
            else
                GameManager.pInstance.playerCharacter02 = pickedCharacter;
        }
    }

	public void pickDocinho()
	{
		GameManager.pInstance.dochinoIsPicked = true;
		GameManager.pInstance.dochinoSelection.color = new Color(0.3f, 0.3f, 0.3f, 0.75f);
	}

	public void pickFlorzinha()
	{
		GameManager.pInstance.florzinhaIsPicked = true;
		GameManager.pInstance.florzinhaSelection.color = new Color(0.3f, 0.3f, 0.3f, 0.75f);
	}

	public void pickLindinha()
	{
		GameManager.pInstance.lindinhaIsPicked = true;
		GameManager.pInstance.lindinhaSelection.color = new Color(0.3f, 0.3f, 0.3f, 0.75f);
	}

    void FixedUpdate()
    {
        if (playerPos == 1)
        {
            playerSelector.transform.position = Vector2.Lerp(playerSelector.transform.position, pos01, 0.4f);
            playerCharacter.GetComponent<SpriteRenderer>().sprite = characters[0];
        }
        else if (playerPos == 2)
        {
            playerSelector.transform.position = Vector2.Lerp(playerSelector.transform.position, pos02, 0.4f);
            playerCharacter.GetComponent<SpriteRenderer>().sprite = characters[1];
        }
        else if (playerPos == 3)
        {
            playerSelector.transform.position = Vector2.Lerp(playerSelector.transform.position, pos03, 0.4f);
            playerCharacter.GetComponent<SpriteRenderer>().sprite = characters[2];
        }

        float inputVelocity = Input.GetAxisRaw(playerAxis);

        if (inputDelay < 0.5f || alreadyPicked)
            return;
        
        if(inputVelocity > 0)
        {
            if (playerPos >= 3)
                playerPos = 1;
            else
                playerPos += 1;

            inputDelay = 0;
        }
        else if(inputVelocity < 0)
        {
            if (playerPos <= 1)
                playerPos = 3;
            else
                playerPos -= 1;

            inputDelay = 0;
        }
    }
}