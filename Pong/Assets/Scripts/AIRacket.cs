﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AIRacket : MonoBehaviour
{
	public Transform ball;
	public float speed;
	public float fakeInput;

	private Rigidbody2D rigid;
	private TrailRenderer trail;

	void Awake ()
	{
		rigid = gameObject.GetComponent<Rigidbody2D> ();
	}

	void Update ()
	{
		if (trail == null) {
			trail = GetComponentInChildren<TrailRenderer> ();
			trail.enabled = false;
		}
	}

	void FixedUpdate ()
	{
		if (!ball) {
			ball = GameObject.Find ("Ball(Clone)").transform;
			return;
		}

		if (ball.position.x > 0) {
			if (ball.position.y > (transform.position.y + ((transform.localScale.y * 3) / 2)))
				rigid.velocity = new Vector2 (0, fakeInput) * speed;
			else if (ball.position.y < (transform.position.y - ((transform.localScale.y * 3) / 2)))
				rigid.velocity = new Vector2 (0, -fakeInput) * speed;
			else
				rigid.velocity = new Vector2 (0, 0);
		}
	}
}