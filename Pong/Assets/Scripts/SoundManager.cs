﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    static public SoundManager instance = null;

    public AudioSource musicController, sfxController;
    public AudioClip pad01, pad02, wall, goal, siren, gameMusic;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            DestroyImmediate(this);
    }

	public void StopAllSound()
	{
		sfxController.Stop ();
	}

    public void playPad01()
    {
        sfxController.clip = pad01;
        sfxController.Play();
    }

    public void playPad02()
    {
        sfxController.clip = pad02;
        sfxController.Play();
    }

    public void playWall()
    {
        sfxController.clip = wall;
        sfxController.Play();
    }

    public void playGoal()
    {
        sfxController.clip = goal;
        sfxController.Play();
    }

    public void playSiren()
    {
        sfxController.clip = siren;
        sfxController.Play();
    }

    public void playGameMusic()
    {
        musicController.clip = gameMusic;
        musicController.Play();
    }

    public void stopGameMusic()
    {
        musicController.Stop();
    }
}