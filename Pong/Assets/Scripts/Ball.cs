﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
	public float orginalSpeed = 65;
	public float speed;
	public GameObject explosionParticle;
	public TrailRenderer trail;

	public bool isControlled;

	private Rigidbody2D rigid;
	private SpriteRenderer sp;

	void Start ()
	{
		rigid = GetComponent<Rigidbody2D> ();
		sp = GetComponent<SpriteRenderer> ();
		rigid.velocity = Vector2.right * orginalSpeed;
		speed = orginalSpeed;
	}

	void FixedUpdate ()
	{
		speed += 0.008f;
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.name == "Player01") {
			// Calculate hit Factor
			float y = hitFactor (transform.position,
				          col.transform.position,
				          col.collider.bounds.size.y);

			// Calculate direction, make length=1 via .normalized
			Vector2 dir = new Vector2 (1, y).normalized;

			speed += 2;

			// Set Velocity with dir * speed
			rigid.velocity = dir * speed;

			SoundManager.instance.playPad01 ();
		}
		else if (col.gameObject.name == "Player02") {
			// Calculate hit Factor
			float y = hitFactor (transform.position,
				          col.transform.position,
				          col.collider.bounds.size.y);

			// Calculate direction, make length=1 via .normalized
			Vector2 dir = new Vector2 (-1, y).normalized;

			speed += 2;

			// Set Velocity with dir * speed
			rigid.velocity = dir * speed;

			SoundManager.instance.playPad02 ();
		} else if (col.gameObject.name == "RightGol") {
			GameManager.pInstance.playerScore01 += 1;

			rigid.velocity = Vector2.zero;
			sp.enabled = false;
			trail.enabled = false;
			explosionParticle.SetActive (true);

			if (!GameManager.pInstance.gameIsOver)
				StartCoroutine (RestartBall ("right", 1));
			
			SoundManager.instance.playGoal ();
		} else if (col.gameObject.name == "LeftGol") {
			GameManager.pInstance.playerScore02 += 1;

			rigid.velocity = Vector2.zero;
			sp.enabled = false;
			trail.enabled = false;
			explosionParticle.SetActive (true);

			if (!GameManager.pInstance.gameIsOver)
				StartCoroutine (RestartBall ("left", 1));

			SoundManager.instance.playGoal ();
		} else if (col.gameObject.name == "Restart") {
			rigid.velocity = Vector2.zero;
			sp.enabled = false;
			trail.enabled = false;

			if (!GameManager.pInstance.gameIsOver)
				StartCoroutine (RestartBall ("left", 0.01f));
		} else
			SoundManager.instance.playWall ();
	}

	IEnumerator RestartBall (string dir, float delay)
	{
		yield return new WaitForSeconds (delay);

		explosionParticle.SetActive (false);
		trail.enabled = true;

		sp.enabled = true;

		Vector3 spawnPosition = new Vector3 (Random.Range (-3.0f, 3.0f), 0.0f, 0.0f);

		Quaternion spawnRotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);

		transform.position = spawnPosition;
		transform.rotation = spawnRotation;

		speed = orginalSpeed;

		if (dir == "left")
			rigid.velocity = Vector2.right * orginalSpeed;
		else if (dir == "right")
			rigid.velocity = Vector2.left * orginalSpeed;
	}

	float hitFactor (Vector2 ballPos, Vector2 racketPos, float racketHeight)
	{
		// ascii art:
		// ||  1 <- at the top of the racket
		// ||
		// ||  0 <- at the middle of the racket
		// ||
		// || -1 <- at the bottom of the racket
		return (ballPos.y - racketPos.y) / racketHeight;
	}
}