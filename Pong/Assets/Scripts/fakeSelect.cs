﻿using UnityEngine;
using System.Collections;

public class fakeSelect : MonoBehaviour
{
	public GameObject playerSelector;
	public GameObject playerCharacter;
	public GameObject opponentCharacter;
	public Vector2 selectedPos;
	public Sprite[] characters;
	public Vector2 pos01, pos02, pos03;
	public int playerPos;
	public string pickedCharacter;

	bool pick = false;

	private bool alreadyPicked;

	void Start()
	{
		StartPick ();
	}

	void FixedUpdate ()
	{
		if (playerPos == 1)
		{
			playerSelector.transform.position = Vector2.Lerp(playerSelector.transform.position, pos01, 0.4f);
			playerCharacter.GetComponent<SpriteRenderer>().sprite = characters[0];
		}
		else if (playerPos == 2)
		{
			playerSelector.transform.position = Vector2.Lerp(playerSelector.transform.position, pos02, 0.4f);
			playerCharacter.GetComponent<SpriteRenderer>().sprite = characters[1];
		}
		else if (playerPos == 3)
		{
			playerSelector.transform.position = Vector2.Lerp(playerSelector.transform.position, pos03, 0.4f);
			playerCharacter.GetComponent<SpriteRenderer>().sprite = characters[2];
		}

		if (pick)
		{
			if (playerPos == 1 && !GameManager.pInstance.dochinoIsPicked) {
				pick = false;
				alreadyPicked = true;
				pickedCharacter = "Dochino";

				playerCharacter.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				playerCharacter.transform.position = Vector2.Lerp (playerCharacter.transform.position, selectedPos, 1);
				GameManager.pInstance.dochinoIsPicked = true;
				GameManager.pInstance.dochinoSelection.color = new Color (0.3f, 0.3f, 0.3f, 0.75f);
			} else if (playerPos == 2 && !GameManager.pInstance.florzinhaIsPicked) {
				pick = false;
				alreadyPicked = true;
				pickedCharacter = "Florzinha";

				playerCharacter.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				playerCharacter.transform.position = Vector2.Lerp (playerCharacter.transform.position, selectedPos, 1);
				GameManager.pInstance.florzinhaIsPicked = true;
				GameManager.pInstance.florzinhaSelection.color = new Color (0.3f, 0.3f, 0.3f, 0.75f);
			} else if (playerPos == 3 && !GameManager.pInstance.lindinhaIsPicked) {
				pick = false;
				alreadyPicked = true;
				pickedCharacter = "Lindinha";

				playerCharacter.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				playerCharacter.transform.position = Vector2.Lerp (playerCharacter.transform.position, selectedPos, 1);
				GameManager.pInstance.lindinhaIsPicked = true;
				GameManager.pInstance.lindinhaSelection.color = new Color (0.3f, 0.3f, 0.3f, 0.75f);
			} else {
				pick = false;
				StartCoroutine (chagePos(.2f));
			}
		}

		if(pickedCharacter != null)
		{
			if (playerSelector.name == "PlayerSelector01")
				GameManager.pInstance.playerCharacter01 = pickedCharacter;
			else
				GameManager.pInstance.playerCharacter02 = pickedCharacter;
		}
	}

	public void StartPick()
	{
		StartCoroutine (chagePos(2f));
	}

	IEnumerator chagePos(float _delay)
	{
		int count = 4;

		yield return new WaitForSeconds (_delay);

		while (count > 0)
		{
			playerPos = Random.Range (1, 3);
			print (playerPos);
			count--;

			yield return new WaitForSeconds (_delay / 2);
		}

		pick = true;

		yield break;
	}

}