﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoveRacket : MonoBehaviour
{
    public float speed;
    public float boostSpeed;
    public string playerAxis = "Vertical";
    public string playerBoost = "PlayerBoost";
    public TrailRenderer trail;

    public int score;

    public Text _scoreText;

	private Rigidbody2D rigid;

	void Start()
	{
		rigid = GetComponent<Rigidbody2D>();
	}

    void Update()
    {
        if(trail == null)
            trail = GetComponentInChildren<TrailRenderer>();
    }
    
    void FixedUpdate()
    {
        float inputVelocity = Input.GetAxisRaw(playerAxis);

        if (GameManager.pInstance.onCharacterSelect)
            return;

            if (Input.GetButton(playerBoost))
            {
				rigid.velocity = new Vector2(0, inputVelocity) * speed * boostSpeed;
                trail.enabled = true;
            }
            else
            {
				rigid.velocity = new Vector2(0, inputVelocity) * speed;
                trail.enabled = false;
            }
    }
}