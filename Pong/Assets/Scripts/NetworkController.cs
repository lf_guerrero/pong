﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(NetworkView))]
public class NetworkController : MonoBehaviour 
{
	public static NetworkController instance;
	public string myName;
	public string opponentName;

	public GameObject selectionCanvas;
	public GameObject inviteCanvas;

	private bool isPlayer01;
	private NetworkView netView;

	void Awake()
	{
		if (!instance)
			instance = this;
		else
			DestroyImmediate(this);

		netView = GetComponent<NetworkView> ();
	}

	void Start()
	{
		//myName = SetName.instance.myName;
	}

	void onConnectedToServer()
	{
		netView.RPC("AddMeToOnline", RPCMode.Server, myName);
	}

	public void SendInvite()
	{
		netView.RPC("RecieveInvite", RPCMode.Others, opponentName, myName);
		isPlayer01 = true;
	}

	[RPC]
	void RecieveInvite(string _nameRecieved, string _opponentName)
	{
		if (_nameRecieved == myName) {
			print ("Yay I got invited");
			_opponentName = opponentName;
			inviteCanvas.SetActive (true);
			isPlayer01 = false;
		}
	}

	public void AcceptedInvite()
	{
		print("accepted invite");
		netView.RPC ("PlayerAcceptedInvite", RPCMode.Others, NetworkController.instance.opponentName, NetworkController.instance.myName);
	}

	[RPC]
	void PlayerAcceptedInvite(string playerName01, string playerName02)
	{
		print ("ISSO FUNCIONA PORRA");
	}

	void InviteFailed()
	{
		//Failed to send/recieve/respond invitation.
	}

	void FailedToJoin()
	{
		//An error occured when the players tried to join the room. Try to create another room, if it fails again return both to the main menu.
	}

	[RPC]
	void CharacterSelection(string playerName01, string playerName02)
	{
		print ("on Character selection");

		//Sync character selection.
		if (isPlayer01 && myName == playerName01) {
			Network.Instantiate (GameManager.pInstance.florzinhaPrefab, GameManager.pInstance.player01.transform.position, GameManager.pInstance.player01.transform.rotation, 0);
			netView.RPC ("PlayerReady", RPCMode.Server, myName, "Player01");
			selectionCanvas.SetActive (false);
		} else if(!isPlayer01 && myName == playerName02){
			Network.Instantiate (GameManager.pInstance.lindinhaPrefab, GameManager.pInstance.player02.transform.position, GameManager.pInstance.player02.transform.rotation, 0);
			netView.RPC ("PlayerReady", RPCMode.Server, myName, "Player02");
			selectionCanvas.SetActive (false);
			inviteCanvas.SetActive(false);
		}
	}

	void InGame()
	{
		//Sync pad pos, ball pos, score, end game.

	}

	void CloseConnection()
	{
		//Close the room once both players have left the room.
	}

	void DisconnectInGame()
	{
		//If a player disconnects take both of them back to the main menu.
	}
}