﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class SetName : MonoBehaviour
{
    public static SetName instance;

    [SerializeField]
    public List<string> nameList = new List<string>();

    public string myName;
    public Button agenciaButton;

    public GameObject agenciaHolder;
    public GameObject BG;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            DestroyImmediate(this);

        DontDestroyOnLoad(gameObject);

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Start()
    { 
        foreach (string s in ReturnPlayerNames())
        {
            Button agenciaPeople = (Button)Instantiate(agenciaButton, transform.position, transform.rotation);
            agenciaPeople.transform.SetParent(agenciaHolder.transform);
            agenciaPeople.GetComponentInChildren<Text>().text = s;
            agenciaPeople.onClick.AddListener(delegate () { SetMyName(agenciaPeople.GetComponentInChildren<Text>().text); });
            agenciaPeople.GetComponent<RectTransform>().localScale = Vector3.one;
        }

        EventSystem.current.SetSelectedGameObject(GameObject.Find("AgenciaButton(Clone)"));
    }

    void SetMyName (string _myName)
    {
        myName = _myName;
        agenciaHolder.SetActive(false);
        BG.SetActive(false);
    }

    public List<string> ReturnPlayerNames()
    {
        List<string> newList = new List<string>();

        foreach (string s in nameList)
        {
            newList.Add(s);
        }

        return newList;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene("StartScreen", LoadSceneMode.Single);
    }
}