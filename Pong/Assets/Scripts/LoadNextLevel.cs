﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadNextLevel : MonoBehaviour 
{
	public void LoadLevel (string levelName) 
	{
		SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Single);
	}
}
