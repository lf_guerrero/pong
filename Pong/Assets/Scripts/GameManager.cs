﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    static public GameManager pInstance = null;
    
    public GameObject GameCanvas;

    public bool dochinoIsPicked;
    public bool florzinhaIsPicked;
    public bool lindinhaIsPicked;
    public SpriteRenderer dochinoSelection;
    public SpriteRenderer florzinhaSelection;
    public SpriteRenderer lindinhaSelection;

    public string playerCharacter01;
    public string playerCharacter02;
    public GameObject dochinoPrefab;
    public GameObject florzinhaPrefab;
    public GameObject lindinhaPrefab;

    public Image playerName01;
    public Image playerName02;
    public Sprite dochinoName;
    public Sprite florzinhaName;
    public Sprite lindinhaName;

    public Image judge;
    public Sprite dochinoJudge;
    public Sprite florzinhaJudge;
    public Sprite lindinhaJudge;

    public bool onCharacterSelect;
    private bool playerPicked01;
    private bool playerPicked02;
    public SpriteRenderer[] characterSelectColors;
    private float a = 1;

    public GameObject uiScoreZone;
    public Font uiScoreFont;
    public GameObject ballPrefab;
    
    public GameObject player01;
    public GameObject player02;
    public int playerScore01;
    public int playerScore02;
	public Text scoreText01;
	public Text scoreText02;

    private float timer;
    public Text timerText;
    
    public bool gameStarted = false;

	public bool gameIsOver = false;
    private bool playerWon01 = false;
    private bool playerWon02 = false;

    public Image[] endScreenColors;
    public Text[] endScreenText;
    public Image endCharacter;
    public Sprite dochinoEnd;
    public Sprite florzinhaEnd;
    public Sprite lindinhaEnd;
    public Image endCharacterName;
    public Sprite dochinoEndName;
    public Sprite florzinhaEndName;
    public Sprite lindinhaEndName;
    public Text scoreText;
    public Text endTimer;
    private float a2 = 0;

	bool canWrite = true;

	public string myName, opponentName;

    public string GameSceneName;

	public bool isLocal;

    void Awake()
    {
		if (!pInstance)
			pInstance = this;
		else
			DestroyImmediate(this);

        onCharacterSelect = true;
    }

	void Start()
	{
		//myName = SetName.instance.myName;
	}

    public void Update()
    {
        if (Input.GetButtonDown("StartGame") && !gameStarted && !onCharacterSelect)
        {
			SpawnBall();
            gameStarted = true;
        }

        if (!onCharacterSelect && !gameIsOver)
        {
            timer += Time.deltaTime;

            int minutes = Mathf.FloorToInt(timer / 60F);
            int seconds = Mathf.FloorToInt(timer - minutes * 60);
            string timeMS = string.Format("{0:00}:{1:00}", minutes, seconds);

            timerText.text = timeMS;
        }

        if(playerScore01 == 5)
        {
            playerWon01 = true;
            gameIsOver = true;
        }
        else if(playerScore02 == 5)
        {
            playerWon02 = true;
            gameIsOver = true;
        }

        if (gameIsOver)
        {
			if (!isLocal) {
				if (canWrite) {
					canWrite = false;
					if (playerWon01)
						SaveLocalFile.instance.finalData = "Horario da partida: " + System.DateTime.Now.TimeOfDay.ToString () + " Vitoria de: " + myName + ". Placar: 5x" + playerScore02 + " Tempo de Jogo: " + timer;
					else
						SaveLocalFile.instance.finalData = "Horario da partida: " + System.DateTime.Now.TimeOfDay.ToString () + " Derrota de: " + myName + ". Placar: 5x" + playerScore01 + " Tempo de Jogo: " + timer;
					
					SaveLocalFile.instance.CollectData ();
				}
			}

            if (playerWon01)
            {
                if (playerCharacter01 == "Dochino")
                {
                    endCharacter.sprite = dochinoEnd;
                    endCharacterName.sprite = dochinoEndName;
                }
                else if (playerCharacter01 == "Florzinha")
                {
                    endCharacter.sprite = florzinhaEnd;
                    endCharacterName.sprite = florzinhaEndName;
                }
                else if (playerCharacter01 == "Lindinha")
                {
                    endCharacter.sprite = lindinhaEnd;
                    endCharacterName.sprite = lindinhaEndName;
                }
            }

            if (playerWon02)
            {
                if (playerCharacter02 == "Dochino")
                {
                    endCharacter.sprite = dochinoEnd;
                    endCharacterName.sprite = dochinoEndName;
                }
                else if (playerCharacter02 == "Florzinha")
                {
                    endCharacter.sprite = florzinhaEnd;
                    endCharacterName.sprite = florzinhaEndName;
                }
                else if (playerCharacter02 == "Lindinha")
                {
                    endCharacter.sprite = lindinhaEnd;
                    endCharacterName.sprite = lindinhaEndName;
                }
            }

            int minutes = Mathf.FloorToInt(timer / 60F);
            int seconds = Mathf.FloorToInt(timer - minutes * 60);
            string timeMS = string.Format("{0:00}:{1:00}", minutes, seconds);
            
            endTimer.text = timeMS;

            if (a2 != 1)
            {
                a2 += 0.05f;

                for (int i = 0; i < endScreenColors.Length; i++)
                    endScreenColors[i].color = new Color(1, 1, 1, a2);

                for (int i = 0; i < endScreenText.Length; i++)
                    endScreenText[i].color = new Color(0, 0, 0, a2);
            }
            else
                Time.timeScale = 0;

            if (Input.anyKeyDown)
                SceneManager.LoadScene(GameSceneName, LoadSceneMode.Single);
        }

        if (!playerPicked01)
        {
            if (playerCharacter01 == "Dochino")
            {
                playerName01.sprite = dochinoName;
                GameObject p01 = (GameObject)Instantiate(dochinoPrefab, player01.transform.position, player01.transform.rotation);
                p01.transform.parent = player01.transform;
                p01.transform.localScale = Vector3.one;
                playerPicked01 = true;
            }
            else if (playerCharacter01 == "Florzinha")
            {
                playerName01.sprite = florzinhaName;
                GameObject p01 = (GameObject)Instantiate(florzinhaPrefab, player01.transform.position, player01.transform.rotation);
                p01.transform.parent = player01.transform;
                p01.transform.localScale = Vector3.one;
                playerPicked01 = true;
            }
            else if (playerCharacter01 == "Lindinha")
            {
                playerName01.sprite = lindinhaName;
                GameObject p01 = (GameObject)Instantiate(lindinhaPrefab, player01.transform.position, player01.transform.rotation);
                p01.transform.parent = player01.transform;
                p01.transform.localScale = Vector3.one;
                playerPicked01 = true;
            }
        }

        if (!playerPicked02)
        {
            if (playerCharacter02 == "Dochino")
            {
                playerName02.sprite = dochinoName;
                GameObject p02 = (GameObject)Instantiate(dochinoPrefab, player02.transform.position, player02.transform.rotation);
                p02.transform.parent = player02.transform;
                p02.transform.localScale = Vector3.one;
                playerPicked02 = true;
            }
            else if (playerCharacter02 == "Florzinha")
            {
                playerName02.sprite = florzinhaName;
                GameObject p02 = (GameObject)Instantiate(florzinhaPrefab, player02.transform.position, player02.transform.rotation);
                p02.transform.parent = player02.transform;
                p02.transform.localScale = Vector3.one;
                playerPicked02 = true;
            }
            else if (playerCharacter02 == "Lindinha")
            {
                playerName02.sprite = lindinhaName;
                GameObject p02 = (GameObject)Instantiate(lindinhaPrefab, player02.transform.position, player02.transform.rotation);
                p02.transform.parent = player02.transform;
                p02.transform.localScale = Vector3.one;
                playerPicked02 = true;
            }
        }

        if (a != 0)
        {
            if (playerCharacter01 != "" && playerCharacter02 != "")
            {
                if (dochinoIsPicked && florzinhaIsPicked)
                    judge.sprite = lindinhaJudge;
                else if (dochinoIsPicked && lindinhaIsPicked)
                    judge.sprite = florzinhaJudge;
                else if (florzinhaIsPicked && lindinhaIsPicked)
                    judge.sprite = dochinoJudge;

                a -= 0.05f;

                for (int i = 0; i < characterSelectColors.Length; i++)
                    characterSelectColors[i].color = new Color(1, 1, 1, a);

                if (a <= 0)
                {
                    GameCanvas.SetActive(true);
                    onCharacterSelect = false;
                    a = 0;
                }
            }
        }
    }

    public void FixedUpdate()
    {
        if (gameStarted)
        {
			scoreText01.text = "" + playerScore01;
			scoreText02.text = "" + playerScore02;

            if (player01.name != "Player01")
                player01.name = "Player01";

            if (player02.name != "Player02")
                player02.name = "Player02";
        }
    }

    public void SpawnBall()
    {
            Vector3 spawnPosition = new Vector3(Random.Range(-3.0f, 3.0f), 0.0f, 0.0f);

            Quaternion spawnRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);

            Instantiate(ballPrefab, spawnPosition, spawnRotation);
    }

	public void GoToEndScreen()
	{
		timer = 30;

		int minutes = Mathf.FloorToInt(timer / 60F);
		int seconds = Mathf.FloorToInt(timer - minutes * 60);
		string timeMS = string.Format("{0:00}:{1:00}", minutes, seconds);

		endTimer.text = timeMS;

		if (a2 != 1)
		{
			a2 += 0.05f;

			for (int i = 0; i < endScreenColors.Length; i++)
				endScreenColors[i].color = new Color(1, 1, 1, a2);

			for (int i = 0; i < endScreenText.Length; i++)
				endScreenText[i].color = new Color(0, 0, 0, a2);
		}
		else
			Time.timeScale = 0;
	}
}