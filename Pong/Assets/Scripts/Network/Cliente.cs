using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System;

[RequireComponent(typeof(NetworkView))]
public class Cliente : MonoBehaviour {

    public static Cliente instance;

    public GameObject MinaObj;

    public void Awake() {
        Cliente.instance = this;
    }

    void Update() {

    }

    public void CriarPlayer() {
        var base_temp = Network.Instantiate(MinaObj, new Vector3(0, 0, 0), Quaternion.identity, 0) as GameObject;

    }

    public void NomesOK(string nomes, string grupo) {
        EnviarMsg("nomesOK", grupo, nomes);
    }


    void SetarNome(NetworkViewID baseID, string grupoNome) {
        GameObject _tmp = NetworkView.Find(baseID).gameObject;
        _tmp.transform.Find(".nameG").GetComponent<TextMesh>().text = grupoNome;
        _tmp.transform.Find(".nameG").GetComponent<MeshRenderer>().enabled = false;
    }

    #region RPC's
    void EnviarMsg(string _msg1, string _msg2, string _msg3) {
        GetComponent<NetworkView>().RPC("RecebeComando", RPCMode.Server, _msg1, _msg2, _msg3);
    }

    void EnviarMsg(string _msg1, string _msg2) {
        GetComponent<NetworkView>().RPC("RecebeComando", RPCMode.Server, _msg1, _msg2, "");
    }

    void EnviarMsg(string _msg1) {
        GetComponent<NetworkView>().RPC("RecebeComando", RPCMode.Server, _msg1, "", "");
    }

    void EnviarMsg(string _msg1, NetworkViewID _msg2) {
        GetComponent<NetworkView>().RPC("RecebeComando2", RPCMode.Server, _msg1, _msg2);
    }

    void EnviarMsg(string _msg1, float _msg2, float _msg3, float _msg4, int _msg5, int _msg6) {
        GetComponent<NetworkView>().RPC("rpc_EnvioMina", RPCMode.Server, _msg1, _msg2, _msg3, _msg4, _msg5, _msg6);
    }

    [RPC]
    void RecebeComando(string _msg1, string _msg2, string _msg3, NetworkMessageInfo info) {
        //Debug.Log("Cliente - "+_msg1 + " - " + _msg2 + " - " + _msg3);		
       
        if (_msg1 == "EndGame") {
            Debug.Log("end");
            //Network.DestroyPlayerObjects(Network.player);
            Application.LoadLevel(Application.loadedLevel);
        }


    }

    [RPC]
    void RecebeComando2(string _msg1, NetworkViewID _msg2, NetworkMessageInfo info) {
    }

    [RPC]
    void RecebeComando3(string _msg1, NetworkViewID _msg2, NetworkViewID _msg3, int _msg4, int _msg5, float _msg6, float _msg7, NetworkMessageInfo info) {
        Debug.Log(_msg1);
    }

    [RPC]
    void RecebeComando4(string _msg1, NetworkViewID _msg2, int _msg3, float _msg4, float _msg5, float _msg6, NetworkMessageInfo info) {
        if (_msg1 == "respRadar") {
        }
    }

    [RPC]
    void rpc_EnvioNomes(string _msg1, NetworkViewID _msg2, string _msg3, NetworkMessageInfo info) {
        if (_msg1 == "nome_grupo") {
            SetarNome(_msg2, _msg3);
        }
    }

    [RPC]
    void rpc_EnvioMina(string _msg1, float _msg2, float _msg3, float _msg4, int _msg5, int _msg6, NetworkMessageInfo info) {
    }
    #endregion
}

