using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System;
using System.IO;

[RequireComponent(typeof(NetworkView))]
public class Servidor : MonoBehaviour
{
    public static Servidor instance;

    public GameObject BolinhaGameObject;

    public List<string> playerList = new List<string>(45) { };

    public NetworkView msg;
    
    public void Awake()
    {
        Servidor.instance = this;
    }

    void Start()
    {
        if (msg == null)
            msg = GetComponent<NetworkView>();
    }
    
     public void Criarbolinha()
    {
        var Obj = Network.Instantiate(BolinhaGameObject, new Vector3(380, 280, 0), Quaternion.identity, 0) as GameObject;
    }

    void OnNetworkInstantiate(NetworkMessageInfo info) {
        Debug.Log(GetComponent<NetworkView>().viewID + " spawned");
    }

    void OnPlayerConnected(NetworkPlayer player) {
        //REQUEST NAME
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
//        for (int i = 0; i < ListaJogadores.Count; i++) {
//            //	if (ListaJogadores[i].ID == player.guid) {
//            ListaJogadores.RemoveAt(i);
//            Network.DestroyPlayerObjects(player);
//            return;
//            //	}			
//        }
    }

    [RPC]
    public void PlayerAcceptedInvite(string player1, string player2)
    {
        print("Accept");
        msg.RPC("CharacterSelection", RPCMode.Others, player1, player2);
    }

    [RPC]
    public void CharacterSelection()
    {

    }

    [RPC]
    public void AddMeToOnline(string playerName)
    {
       for(int i = 0; i < playerList.Count; i++)
        {
            if (playerList[i] == null)
            {
                playerList[i] = playerName;
                break;
            }
            continue;
        }
    }

    [RPC]
    void PlayerReady()
    {

    }

    void EnviarMsg(string _msg1, string _msg2, string _msg3) {
        GetComponent<NetworkView>().RPC("RecebeComando", RPCMode.Others, _msg1, _msg2, _msg3);
    }

    void EnviarMsg(NetworkPlayer player, string _msg1, string _msg2, string _msg3) {
        GetComponent<NetworkView>().RPC("RecebeComando", player, _msg1, _msg2, _msg3);
    }

    void EnviarMsg(NetworkPlayer player, NetworkViewID _msg1, NetworkViewID _msg2) {
        GetComponent<NetworkView>().RPC("RecebeComando", player, _msg1, _msg2);
    }

    void EnviarMsg(NetworkPlayer player, string _msg1, NetworkViewID _msg2, NetworkViewID _msg3, int _msg4, int _msg5, float _msg6, float _msg7) { /// USADO PARA ENVIAR CARRINHO VIRTUAL, MINA
        GetComponent<NetworkView>().RPC("RecebeComando3", player, _msg1, _msg2, _msg3, _msg4, _msg5, _msg6, _msg7);
    }

    void EnviarMsg(NetworkPlayer player, string _msg1, NetworkViewID _msg2, int _msg3, float _msg4, float _msg5, float _msg6) { /// USADO PARA ENVIAR RADAR
        GetComponent<NetworkView>().RPC("RecebeComando4", player, _msg1, _msg2, _msg3, _msg4, _msg5, _msg6);
    }

    void EnviarMsg(string _msg1, NetworkViewID _msg2, string _msg3) {   /// USADO PARA ENVIAR NOME DOS GRUPOS	
        GetComponent<NetworkView>().RPC("rpc_EnvioNomes", RPCMode.All, _msg1, _msg2, _msg3);
    }

    [RPC]
    void RecebeComando(string _msg1, string _msg2, string _msg3, NetworkMessageInfo info) {
        //string[] InfoReceived = DADOS.Split(new char[] {','});
        Debug.Log(_msg1);
       
        if (_msg1 == "nomesOK") {
           // NomesOK(info.sender, _msg2, _msg3);
        }
    }

    [RPC]
    void RecebeComando2(string _msg1, NetworkViewID _msg2, NetworkMessageInfo info) {
       if (_msg1 == "enviarMissel") {
            //EnviarCarrinhoEnergia(info.sender, _msg2);
        }
    }

    [RPC]
    void rpc_EnvioMina(string _msg1, float _msg2, float _msg3, float _msg4, int _msg5, int _msg6, NetworkMessageInfo info) {
        if (_msg1 == "enviarMina") {
            // EnviarMina(info.sender, _msg2, (int)_msg3, _msg4, _msg5, _msg6);
        }
    }

    [RPC]
    void rpc_EnvioNomes(string _msg1, NetworkViewID _msg2, string _msg3, NetworkMessageInfo info) {
    }

    [RPC]
    void RecebeComando3(string _msg1, NetworkViewID _msg2, NetworkViewID _msg3, int _msg4, int _msg5, float _msg6, float _msg7, NetworkMessageInfo info) {
    }

    [RPC]
    void RecebeComando4(string _msg1, NetworkViewID _msg2, int _msg3, float _msg4, float _msg5, float _msg6, NetworkMessageInfo info) {
    }

}