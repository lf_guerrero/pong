﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Network_Class : MonoBehaviour
{
    public static Network_Class instance;

    public int connectionPort = 3001;
    int NumeroDeJogadores = 32;

    private bool refreshing = false;
    private bool connecting = false;
    private int Tentativas = 0;

    public string IP = "104.41.36.160";
    public GameObject IpGameObject;

	private int checkIfConnected = 0;

    public void Awake()
    {
        Network_Class.instance = this;
    }

    void Start() 
	{

        IP = Network.player.ipAddress;
        InicioServidor();
        //CheckIfOnline();
        //InvokeRepeating("InicioCliente", 0.5f, 5);	
    }
    
    private void Update()
    {

    }

    void CheckIfOnline()
    {
        //Debug.Log(Network.peerType.ToString());
        //if (Network.isClient)
        //{
        //    Destroy(GetComponent<Servidor>());
        //}
        //else if (Network.isServer)
        //{
        //    Destroy(GetComponent<Cliente>());
        //}
    }

    void InicioServidor()
    {
        Network.InitializeServer(NumeroDeJogadores, connectionPort, false);
    }
    
    void InicioCliente()
    {
		if (Network.isClient)
        {
			CancelInvoke ();
			return;
		} else {
			if (checkIfConnected < 7)
            {
				Network.Connect (IP, connectionPort);
				Debug.Log ("Connecting");
				checkIfConnected++;
			}
		}
    }

    public void CriarServidor()
    {
        if (checkIfConnected < 7)
        {
            Network.Connect(IP, connectionPort);
            Debug.Log("Connecting");
            checkIfConnected++;
        }
    }
    
    void OnServerInitialized()
    {
        Debug.Log("Server initialized and ready");
        //Destroy(GetComponent<Cliente>());
    }

    void OnPlayerConnected(NetworkPlayer player)
    {

    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {

    }

    void OnConnectedToServer()
    {
        Debug.Log("Connected to server");
        Destroy(GetComponent<Servidor>());
        connecting = false;
    }
}