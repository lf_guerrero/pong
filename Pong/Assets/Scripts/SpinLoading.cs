﻿using UnityEngine;
using System.Collections;

public class SpinLoading : MonoBehaviour
{
	void Update ()
    {
	    transform.Rotate(Vector3.back * 10 * Time.deltaTime);
    }
}
