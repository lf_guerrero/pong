﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class InviteCanvas : MonoBehaviour 
{
	private NetworkView netView;

	void Awake()
	{
		netView = GetComponent<NetworkView> ();
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Space))
			NetworkController.instance.AcceptedInvite ();
	}
}