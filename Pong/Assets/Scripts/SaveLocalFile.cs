﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class SaveLocalFile : MonoBehaviour
{
	public static SaveLocalFile instance;

	private string Path;

	public string finalData;

	string fileName = "_userName";

	void Awake()
	{
		if (!instance)
			instance = this;
		else
			DestroyImmediate(this);

        SaveData();
    }

	public void CollectData()
	{
		fileName = SetName.instance.myName;

		SaveData();
	}

	public void SaveData()
	{
		Path = @"C:\PongGameResult\" + fileName + ".txt";

		System.IO.FileInfo file = new System.IO.FileInfo(Path);
		file.Directory.Create();

		System.IO.File.WriteAllText (Path, finalData);
	}
}