﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class GameModeMenu : MonoBehaviour
{
	public GameObject telRelogio, novaChamada, meuNome, opponente, vs;
	public Text _clock;
	bool waitForPlayer = false;

    private float timer;
    
	[SerializeField]List<string> _senhaMultplayer = new List<string>(){"y","y","y","y","x"};
	[SerializeField]List<string> _senhaSirene = new List<string>(){"x","x","x","x","y"};

	string myInput;
    int inputCounter = 0;
    int inputCounter2 = 0;

    void Update()
    {
		if (Input.GetButtonDown ("YButton")) {
			myInput = "y";
			CheckInputsMultiplayer ();
			CheckInputSirene ();

		} else if (Input.GetButtonDown ("XButton")) {
			myInput = "x";
			CheckInputsMultiplayer ();
			CheckInputSirene ();
		}

		if (waitForPlayer) {

			if (Input.anyKeyDown) {

                SoundManager.instance.StopAllSound();

				SceneManager.LoadSceneAsync ("SelecaoOponente", LoadSceneMode.Single);
            }
		}
    }

	public void CheckInputSirene()
	{
		switch (inputCounter2)
		{
		case 0:
            if (myInput == _senhaSirene[inputCounter2])
                inputCounter2 += 1;
            else
                inputCounter2 = 0;
			break;

		case 1:
			if (myInput == _senhaSirene [inputCounter2])
				inputCounter2 += 1;
			else
				inputCounter2 = 0;
			break;

		case 2:
			if (myInput == _senhaSirene [inputCounter2])
				inputCounter2 += 1;
			else
				inputCounter2 = 0;
			break;

		case 3:
			if (myInput == _senhaSirene [inputCounter2])
				inputCounter2 += 1;
			else
				inputCounter2 = 0;
			break;

		case 4:
			if(myInput == _senhaSirene [inputCounter2])
			{
				//display na tela com o timer
				telRelogio.SetActive (true);

				// play sound
				SoundManager.instance.playSiren();

				meuNome.SetActive(false);
				opponente.SetActive(false);
				vs.SetActive (false);

                Invoke ("WaitForPlayer", 1f);
				StartCoroutine (CallTimer2 ());

				inputCounter2 = 0;
			}
			break;
		}
	}

	public void CheckInputsMultiplayer()
	{
		switch (inputCounter)
		{

		case 0:
			if (myInput == _senhaMultplayer [inputCounter])
				inputCounter += 1;
			else
				inputCounter = 0;
			break;

		case 1:
			if (myInput == _senhaMultplayer [inputCounter])
				inputCounter += 1;
			else
				inputCounter = 0;
			break;

		case 2:
			if (myInput == _senhaMultplayer [inputCounter])
				inputCounter += 1;
			else
				inputCounter = 0;
			break;

		case 3:
			if (myInput == _senhaMultplayer [inputCounter])
				inputCounter += 1;
			else
				inputCounter = 0;
			break;

		case 4:
			if (myInput == _senhaMultplayer [inputCounter])
			{
				SceneManager.LoadSceneAsync ("SelecaoOponente", LoadSceneMode.Single);
				inputCounter = 0;
			}
			break;
		}
	}

	void WaitForPlayer()
	{
		waitForPlayer = true;
	}

	public IEnumerator CallTimer2()
	{
		timer = 600;

		while (timer > 0)
		{
			timer -= 1;

			if (timer > 300) {
				int minutes = Mathf.FloorToInt ((timer / 2) / 60F);
				int seconds = Mathf.FloorToInt ((timer / 2) - minutes * 60);
				string timeMS = string.Format ("{0:00}:{1:00}", minutes, seconds);

				_clock.text = timeMS;

			} else {

				int minutes = Mathf.FloorToInt ((timer) / 60F);
				int seconds = Mathf.FloorToInt ((timer) - minutes * 60);
				string timeMS = string.Format ("{0:00}:{1:00}", minutes, seconds);

				novaChamada.SetActive (true);
				_clock.text = timeMS;
			}

			yield return new WaitForSeconds (1f);
		}

		if (waitForPlayer) {

			// PERDEU POR TEMPO
			SceneManager.LoadSceneAsync ("Menu", LoadSceneMode.Single);

			yield break;
		} else
			yield break;
	}

}