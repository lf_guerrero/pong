﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class PlayerList : MonoBehaviour 
{
	public Button agenciaButton;

	public GameObject doubleConfirm;
	public GameObject waitScreen;

	public Text newName;

	[SerializeField]
	public List<string> nameList = new List<string>();

	void Start () 
	{
		foreach (string s in nameList) 
		{
			//if (s != SetName.instance.myName) {
				Button agenciaPeople = (Button)Instantiate (agenciaButton, transform.position, transform.rotation);
				agenciaPeople.transform.SetParent (transform);
				agenciaPeople.GetComponentInChildren<Text> ().text = s;
				agenciaPeople.onClick.AddListener (delegate() {
					SetOpponent (agenciaPeople.GetComponentInChildren<Text> ().text);
				});
				agenciaPeople.GetComponent<RectTransform> ().localScale = Vector3.one;
			//}
		}

		EventSystem.current.SetSelectedGameObject(GameObject.Find("AgenciaButtonLobby(Clone)"));
	}

	void Update()
	{
		if (doubleConfirm.activeInHierarchy) 
		{
			if (Input.GetButtonDown("BButton")) {
				print ("Cancel Opponent!");

				GameManager.pInstance.opponentName = "";
//				MultiplayerFlow.instance.isOpponentSelected = false;
				EventSystem.current.SetSelectedGameObject(GameObject.Find("AgenciaButtonLobby(Clone)"));

                doubleConfirm.SetActive (false);
			}

			if(Input.GetButtonDown("CharacterSelectAccept"))
			{
				print ("Confirmed Opponent!");

				//Invoke ("LoadScene", 2);

				doubleConfirm.SetActive (false);
				waitScreen.SetActive (true);
			}
		}
	}
	
	// Update is called once per frame
	void SetOpponent (string _opponentName) 
	{
        Invoke("EnableCheck", 0.2f);
		
		newName.text = _opponentName;

		NetworkController.instance.opponentName = _opponentName;
		NetworkController.instance.SendInvite ();

        EventSystem.current.SetSelectedGameObject(null);
	}

    void EnableCheck()
    {
		CancelInvoke ();
        doubleConfirm.SetActive(true);
    }
}