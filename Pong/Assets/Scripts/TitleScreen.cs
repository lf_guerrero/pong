﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TitleScreen : MonoBehaviour
{
    public string nextSceneName;
    private bool canLoadNextLevel = false;
	private bool lockLoad = false;

	void Update ()
    {
        if (SetName.instance.myName != "")
        {
            Invoke("enableLoad", 0.15f);
        }

        if(canLoadNextLevel && !lockLoad)
        {
            if (Input.anyKey)
            {
				lockLoad = true;
				SceneManager.LoadSceneAsync(nextSceneName, LoadSceneMode.Single);
            }
        }
    }

    void enableLoad()
    {
        canLoadNextLevel = true;
    }
}